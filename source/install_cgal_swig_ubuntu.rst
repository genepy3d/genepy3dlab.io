Install CGAL-SWIG Python Biding in Ubuntu 22.04
===============================================

We tested the CGAL-SWIG v5.5.1. It is compatible with:

- CGAL 5.5.1
- SWIG 4.0

Prerequisite
------------

.. code-block:: bash

    apt-get update
    apt-get install -y build-essential automake cmake libeigen3-dev

Install CGAL 5.5.1 from source
------------------------------

If you installed previous version of CGAL by cmake, to remove it, in the CGAL folder: 

.. code-block:: bash

    sudo make uninstall

Then download the source code of CGAL 5.5.1 from here

`<https://www.cgal.org/2022/10/12/cgal551>`_

In the extracted folder:

.. code-block:: bash

    mkdir build
    cd build
    cmake ..
    sudo make install

Once done, the CGAL lib is often at: ``/usr/local/lib/cmake/CGAL``

Install SWIG 4.0 from apt
-------------------------

The default swig version in ubuntu 22.04 is 4.0.2 which was already compatible

.. code-block:: bash

    sudo apt-get install swig

Install CGAL-SWIG 5.5.1 from source
-----------------------------------

Download the source code here:

`<https://github.com/CGAL/cgal-swig-bindings/releases/tag/v5.5.1.post202210141207>`_

In the extracted folder:

.. code-block:: bash

    cmake -DCGAL_DIR=/usr/local/lib/cmake/CGAL -DBUILD_JAVA=OFF
    make -j 4

Then the default python-biding lib is in ``<CGAL-SWIG DIR>/build-python``

Add python-biding to PYTHONPATH
-------------------------------

We should add the python-binding lib into PYTHONPATH. To add it permanently, add this line to ``.bashrc`` file from your home folder:

.. code-block:: bash

    export PYTHONPATH="$PYTHONPATH:<CGAL-SWIG DiR>/build-python"
