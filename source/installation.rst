Installation
============

GeNePy3D
--------

GeNePy3D is made up of two packages for licensing reasons (more info on `Licenses <index.rst#Licenses>`_). The main package is under the BSD license and can be installed with pip:

    ``pip install genepy3d``

This package covers almost the functions of the library and would be enough to start. 

Another package is under the GPL license and can be installed with:

    ``pip install genepy3d-gpl``

Dependencies of this GPL package are taken care of, except:

- `Mayavi <https://docs.enthought.com/mayavi/mayavi/>`_
- `CGAL <https://cgal.org>`_

Mayavi
------

We used Mayavi to provide a VTK-based interactive visualization of 3D data. It is implemented in the module ``genepy3d_gpl.util.mayavi``. If you want to use this module, please install Mayavi and PyQt5:

    ``pip install mayavi``

    ``pip install PyQt5``

More detail on the installation of Mayavi is `here <https://docs.enthought.com/mayavi/mayavi/installation.html#installing-with-pip>`_.

CGAL
----

Another GPL software is CGAL, a C++ library for computational geometry algorithms. We aim at using CGAL to handle advanced operation and interaction between objects, for example the interection between curve and surface, points and surface, etc. Many other useful functions from CGAL are planned to be added into our library. Please refer to `this page <cgal_list_funcs>`_ for the list of CGAL-used functions in GeNePy3D_GPL. If you want to use these functions, please follow `this instruction <https://github.com/CGAL/cgal-swig-bindings>`_ for the installation of CGAL, SWIG and CGAL_SWIG_Bindings. Also note that the lastest version of GeNePy3D_GPL is compatible with **CGAL 5.5 and SWIG 4.0**. If you use Linux, we wrote a short guideline for installation of these packages under Ubuntu 22.04:

`Installation of CGAL-SWIG-PYTHON-BIDING in Linux <install_cgal_swig_ubuntu>`_.



..  COMMENT
    Installation of CGAL and its Python binding can be sometime cumbersome. We also provided **Docker containers** to facilitate the use of GeNePy3D.

..  COMMENT
    Docker containers
    ------------------
    
    **[TODO: update the docker to the latest version of genepy3d, genepy3d-gpl, CGAL 5.0, SWIG 4.0, and try to include Mayavi...]**
    
    The Docker containers of the GeNePy3D can be found `here <https://gitlab.com/genepy3d/genepy3d_dockers>`_. They are based on the jupyter notebook image and provide a convenient way to have a jupyter notebook running in a ready to use environemnent. Many tutorials and howtos are available to explains the basics, for example `here <https://www.dataquest.io/blog/docker-data-science/>`_.
    
    We provide two containers, one with everything and one excluding the GPL bits. Once you have installed docker, run, from your work directory:
    
        ``docker run -it -p 10000:8888 -v "$PWD":/home/jovyan/work ac744/genepy3d:full``
    
    and it will (download if needed and) run a docker image with GeNePy3D and all it's dependency including CGAL and CGAL_SWIG_Bindings. Just copy paste the link that will appear on the terminal to you browser, changing port 8888 to 10000 and you are good to go! The change made to files in your work directory will remain after you close the container.

GeNePy3D release notes
----------------------

The details of GeNePy3D versions can be found in this `page <genepy3d_versions>`_.
   
   

