Applications and examples
=========================

Zebrafish brain
---------------

.. toctree::
   :maxdepth: 1
   
   application_zebrafish

MNTB
----

TBA.

Folium THG
----------

TBA.



.. **Local 3D scale of a neuronal trace**

.. Trajectory of a neuronal arbor in 3D can be decomposed into sequences of local sections classifying as 1D, 2D or 3D.
.. A local 3d scale is calculated from such decomposition and can be used to study the spatial local structures of the neuronal trace. 

.. .. figure:: _static/figures/retinaganglion_localscale.png
..     :align: center
..     :height: 200px
..     :alt: local_3d_scale
..     :figclass: align-center

..     Local 3D scale of retinal ganglion cell (Badea TC and Nathans J, 2011) is small at straight portion (long axonal part in blue) and high at tortuous portion (axonal part close to the cell body in red).

.. Detail of using the local 3d scale in analysing the neuronal traces was published `here <https://doi.org/10.1371/journal.pcbi.1010211>`_. The notebooks from the publication with examples illustrating the utility of this measurement in practice, and examplifying the use of GeNePy3D, are shown in:

.. https://gitlab.com/msphan/multiscale-intrinsic-dim/ 

.. If you use this metric, please cite the following paper:

.. * Phan MS, Matho K, Beaurepaire E, Livet J, Chessel A (2022). *nAdder: A scale-space approach for the 3D analysis of neuronal traces*. PLOS Computational Biology 18(7): e1010211. `doi:10.1371/journal.pcbi.1010211 <https://doi.org/10.1371/journal.pcbi.1010211>`_


.. **Additional examples**

.. More examples, in the form of jupyter notebooks, are available in the `genepy3d_examples <https://gitlab.com/genepy3d/genepy3d_examples>`_.