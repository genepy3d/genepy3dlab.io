GeNePy3D release notes 
======================

**GeNePy3D 0.3.0**

* Introduce ``SimpleTrack`` to handle track object.
* Change from_points_alpha_shape() using ``alphashape`` package.
* Add new functions to ``Surface``.
* Suport reading ``ims`` file for ``Tree``.

**GeNePy3D 0.2.3**

* New implementation of the Earth Mover's Distance with POT library. The installation of pyemd was removed. Only CGAL that need to install separaly in case using genepy3d_gpl. 

**GeNePy3D 0.2.2**

* Support 2D in some functions of Curve and Tree.
* Provide two versions of local 3d scale, one computed from list of radii of curvatures and one computed from list of sigma values of Gaussian.
* Add ESWC reader for Tree.


**GeNePy3D 0.2.1**

First released version.
