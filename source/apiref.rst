API reference
=============

A full documentation is available in readthedocs for the two packages. 

Reference documentation of genepy3d package, which holds the main library:
`genepy3d API reference  <https://genepy3d.readthedocs.io/>`_

Reference documentation of genepy3d-gpl package, which holds functions linking out to GPL licenced code:
`genepy3d-gpl API reference  <https://genepy3d-gpl.readthedocs.io/>`_