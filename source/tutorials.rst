Tutorials
=========

Some tutorials are provided to give you insights into the use of the library. Full documentation can be found in `API reference <apiref>`_.

.. toctree::
   :maxdepth: 1
   
   working_with_curve
   working_with_tree
   working_with_points
   working_with_surface
   working_with_track
   interaction_objects
   visualization_mayavi
