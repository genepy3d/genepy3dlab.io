Development
===========

GeNePy3D is being developed at the Laboratory for Optics and Biosciences, at Ecole Polytechnique part of the Insitut Polytechnique de Paris. 
Lead developers are `Anatole Chessel <https://portail.polytechnique.edu/lob/en/anatole-chessel>`_ and `Minh-Son Phan <https://research.pasteur.fr/en/member/minh-son-phan/>`_. You can also use the `image.sc <https://image.sc>`_ forums for help or to get in touch.

The library is in relatively early stage and should be considered in alpha. We have tried to organise (a little) our thoughts on where it could/should go in the near and less near future here:

.. toctree::
   :maxdepth: 1
    
   road_map

If you want to contribute please check some preliminary developers documentations below. You can also get in touch though mail, gitlab issues or the `image.sc <https://image.sc>`_ forums.

.. toctree::
   :maxdepth: 1
    
   developer_guide