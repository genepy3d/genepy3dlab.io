.. genepy3d documentation master file, created by
   sphinx-quickstart on Wed Sep 18 15:44:48 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

What is GeNePy3D?
=================

GeNePy3D is a **python library** for **quantitative geometry**, in particular in the context of quantitative microscopy. It aims at providing an easy interface to perform complex data science workflow on geometric data, **manipulating points cloud, surfaces, curves and trees**, converting between them, and performing analysis tasks to extract information and knowledge out of raw geometrical data.

It aims at being a 'middleware' library, that is linking out to recognised and established specialised library when needed but providing easy to use interfaces. The aim is to democratise access to methods from a wide range of mathematical sub-discipline to biological data scientist and computational biologist. The main code repositories can be found `on gitlab <https://gitlab.com/genepy3d/>`_.

If you use GeNePy3D, please cite the following paper:

   Phan MS and Chessel A. *GeNePy3D: a quantitative geometry python toolbox for bioimaging*. F1000Research 2021, 9:1374. `doi:10.12688/f1000research.27395.2 <https://f1000research.com/articles/9-1374/v2>`_

..  list-table::
    :align: center
    :width: 70%

    *   -   ..  figure:: _static/figures/curve_ink.png
                :width: 80%
                :align: center
    
                Curve in 3D, colored code by its curvatures.

        -   ..  figure:: _static/figures/tree_ink.png
                :width: 54%
                :align: center
    
                Tree in 3D, colored code by its distinct types.

    *   -   ..  figure:: _static/figures/surface_ink.png
                :width: 70%
                :align: center
    
                Surface in 3D, computed by alpha shape algorithm.

        -   ..  figure:: _static/figures/points_ink.png
                :width: 55%
                :align: center
    
                Points in 3D, the arrows show its PCA vectors.

User guide
==========

.. toctree::
   :maxdepth: 1

   installation
   gettingstart
   tutorials
   applications
   apiref
   developer_guide
   road_map

Licenses
========

GeNePy3D is part of the larger scipy ecosystem in python, and as such is released under the permissive BSD license to facilitate its use; arguments for using BSD in scientific sotfware can be found for example `here <https://www.astrobetter.com/blog/2014/03/10/the-whys-and-hows-of-licensing-scientific-code/>`_.

But some software directly in the scope of GeNePy3D mission are released under the GPL license. To still be able to use it we separated functions linking out to the GPL software in a distinct repository installed with a different package, `GeNePy3D_GPL <https://gitlab.com/genepy3d/genepy3d_gpl>`_. The distinction is unimportant in practice for academic use and more generally if you do not plan to release software but needed to be made.

Funding
=======

This development was originaly supported by Agence Nationale de la Recherche (contract ANR-11-EQPX-0029 Morphoscope2).  


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`search`
