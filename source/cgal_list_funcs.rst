List of main functions in GENEPY3D_GPL using CGAL 
=================================================

Package ``genepy3d_gpl.interact``

- Module ``curvesurface``

    - ``intersect(crv, surf)``: Intersection between curve and surface.
    - ``inonout(crv, surf)``: Return curve segments inside/lying on/outside of the surface.

- Module ``pointsurface``

    - ``inonout(pnts, surf, ...)``: Check points inside, lying on or outside of the surface.

Package ``genepy3d_gpl.obj``

- Module ``curves``

    - ``intersect(c1, c2)``: Intersection between two curves.

- Module ``points``

    - ``process(pnts, ...])``: Process the points cloud by outlier removal and smoothing.








